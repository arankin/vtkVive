** BETA/ALPHA code here, feel free to use and improve **

## Compiling

This module is part of [VTK](vtk.org) and relies on two external
libraries. You will  need to download/compile/install
[SDL2](https://wiki.libsdl.org/Installation) as well as
[OpenVR](https://github.com/ValveSoftware/openvr). The CMake
build process for this module will try to find those two
libraries. Build an optimized version as you will need the
performance.

## What is included

These classes are designed to be drop in replacements for VTK. As long
as you link your executable to this module the object factory
mechanism should replace the core rendering classes such as
vtkRenderWindow with OpenVR specialized versions. The goal is for VTK
programs to be able to use the OpenVR library with little to no
changes.

vtkOpenVRRenderWindow handles the bulk of interfacing to OpenVR. It supports one
renderer currently. The renderer is assumed to cover the entire window
which is what makes sense in VR. Overlay renderers can probably be
made to work with this but consider how overlays will appear in a
HMD if they do not track the viewpoint etc. This class is based on
sample code from the OpenVR project so it may not be completely ideal
for VTK in its current form.

The vtkOpenVRCamera gets the
matrices from OpenVR to use for rendering.  It contains a scale and
translation that are designed to map world coordinates into the HMD
space (meters around 0,0,0 I believe) This way world coordinates can
be kept in the  units that makes sense for your problem domain and the
camera will handle shifting and scaling those coordinates into the
units that make sens for the HMD.

vtkOpenVRRenderer exists to make ResetCamera  do something
reasonable. In this case it computes a reasonable scale and
translation and sets that on the OpenVRCmaera. It also sets the
default clipping range expansion to be more reasonable as the VTK
default is way too generous.

Supporting interaction is a little odd. All of VTK is designed to pick
and interact based on  X,Y mouse window coordinates. With OpenVR we
actually have XYZ world coordinates and WXYZ orientations. The
vtkOpenVRRenderWindowInteractor catches controller events and converts
them to mouse window events (yuck) but it also stores the world
coordinate  positions and orientations for styles/pickers that can use
them (yay). It supports multiple controllers through the standard
PointerIndex approach used already by VTK for multitouch.

To work with the vtkOpenVRRenderWindowInteractor we have a
vtkInteractorStyleOpenVR that uses world coordinate XYZ position, WXYZ
orientation to adjust actors.  This provides a nice grab and move
style of interaction that is common to OpenVR.

To determine what prop has been picked we have a vtkOpenVRPropPicker that
uses the XYZ world coordinate as the picking value as opposed to
intersecting a ray which is slower etc.

## TODO

there are a few areas that are ripe for improvement.

-  Add support for the OpenVR overlay, it is a great way to display a
  unser interface and OpenVR has a lot of support built in for it.

-  Think seriously about picking and interactors/widgets in the context
  of having a XYZ position at the beginning of the pick operation.

-  Performance, make it faster.

-  Performance, make sure long operations happen asynchronously.

-  Add more event interactions
