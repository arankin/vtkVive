/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkOpenVRPropPicker.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkOpenVRPropPicker - pick an actor/prop given XYZ coordinates
// .SECTION Description
// vtkOpenVRPropPicker is used to pick an actor/prop given a selection
// point in world coordinates.
// This class determines the actor/prop and pick position in world
// coordinates; point and cell ids are not determined.

// .SECTION See Also
// vtkPicker vtkWorldPointPicker vtkCellPicker vtkPointPicker

#ifndef vtkOpenVRPropPicker_h
#define vtkOpenVRPropPicker_h

#include "vtkOpenVRModule.h" // For export macro
#include "vtkAbstractPropPicker.h"

class vtkProp;

class VTKOPENVR_EXPORT vtkOpenVRPropPicker : public vtkAbstractPropPicker
{
public:
  static vtkOpenVRPropPicker *New();

  vtkTypeMacro(vtkOpenVRPropPicker, vtkAbstractPropPicker);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Perform the pick and set the PickedProp ivar. If something is picked, a
  // 1 is returned, otherwise 0 is returned.  Use the GetViewProp() method
  // to get the instance of vtkProp that was picked.  Props are picked from
  // the renderers list of pickable Props.
  int PickProp(double selectionX, double selectionY,
    double selectionZ, vtkRenderer *renderer);

  // Description:
  // Perform a pick from the user-provided list of vtkProps and not from the
  // list of vtkProps that the render maintains.
  int PickProp(double selectionX, double selectionY,
    double selectionZ, vtkRenderer *renderer,
    vtkPropCollection* pickfrom);

  // Description:
  // Overide superclasses' Pick() method.
  int Pick(double selectionX, double selectionY, double selectionZ,
           vtkRenderer *renderer);
  int Pick(double selectionPt[3], vtkRenderer *renderer)
    { return this->Pick( selectionPt[0],
                         selectionPt[1], selectionPt[2], renderer); }

protected:
  vtkOpenVRPropPicker();
  ~vtkOpenVRPropPicker();

  void Initialize();

  vtkPropCollection* PickFromProps;

 private:
  vtkOpenVRPropPicker(const vtkOpenVRPropPicker&);  // Not implemented.
  void operator=(const vtkOpenVRPropPicker&);  // Not implemented.
};

#endif
