/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkInteractorStyleOpenVR.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkInteractorStyleOpenVR - manipulate objects in the scene independent of each other
// .SECTION Description
// vtkInteractorStyleOpenVR allows the user to interact with (rotate,
// pan, etc.) objects in the scene indendent of each other. It is designed
// to use 3d positions and orientations instead of 2D
//
// .SECTION See Also
// vtkInteractorStyleTrackballCamera vtkInteractorStyleJoystickActor
// vtkInteractorStyleJoystickCamera

#ifndef vtkInteractorStyleOpenVR_h
#define vtkInteractorStyleOpenVR_h

#include "vtkOpenVRModule.h" // For export macro
#include "vtkInteractorStyle.h"

class vtkOpenVRPropPicker;
class vtkProp3D;
class vtkMatrix3x3;

class VTKOPENVR_EXPORT vtkInteractorStyleOpenVR : public vtkInteractorStyle
{
public:
  static vtkInteractorStyleOpenVR *New();
  vtkTypeMacro(vtkInteractorStyleOpenVR,vtkInteractorStyle);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Event bindings controlling the effects of pressing mouse buttons
  // or moving the mouse.
  virtual void OnMouseMove();
  virtual void OnLeftButtonDown();
  virtual void OnLeftButtonUp();
  virtual void OnRightButtonDown();
  virtual void OnRightButtonUp();

  // This method handles updating the prop based on changes in the devices
  // pose. We use rotate as the state to mean adjusting-the-actor-pose
  virtual void Rotate();

  // This method handles updating the camera based on changes in the devices
  // pose. We use Dolly as the state to mean moving the camera forward
  virtual void Dolly();

protected:
  vtkInteractorStyleOpenVR();
  ~vtkInteractorStyleOpenVR();

  void FindPickedActor(double x, double y, double z);

  void Prop3DTransform(vtkProp3D *prop3D,
                       double *boxCenter,
                       int NumRotation,
                       double **rotate,
                       double *scale);

  vtkOpenVRPropPicker *InteractionPicker;
  vtkProp3D *InteractionProp;
  vtkMatrix3x3 *TempMatrix3;

private:
  vtkInteractorStyleOpenVR(const vtkInteractorStyleOpenVR&);  // Not implemented.
  void operator=(const vtkInteractorStyleOpenVR&);  // Not implemented.
};

#endif
