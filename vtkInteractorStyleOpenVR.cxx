/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkInteractorStyleOpenVR.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkInteractorStyleOpenVR.h"

#include "vtkCallbackCommand.h"
#include "vtkMath.h"
#include "vtkObjectFactory.h"
#include "vtkOpenVRPropPicker.h"
#include "vtkOpenVRRenderWindowInteractor.h"
#include "vtkProp3D.h"
#include "vtkQuaternion.h"
#include "vtkRenderer.h"
#include "vtkMatrix3x3.h"
#include "vtkOpenVRCamera.h"

vtkStandardNewMacro(vtkInteractorStyleOpenVR);

//----------------------------------------------------------------------------
vtkInteractorStyleOpenVR::vtkInteractorStyleOpenVR()
{
  this->InteractionProp = NULL;
  this->InteractionPicker = vtkOpenVRPropPicker::New();
  this->TempMatrix3 = vtkMatrix3x3::New();
}

//----------------------------------------------------------------------------
vtkInteractorStyleOpenVR::~vtkInteractorStyleOpenVR()
{
  this->InteractionPicker->Delete();
  this->TempMatrix3->Delete();
}

//----------------------------------------------------------------------------
void vtkInteractorStyleOpenVR::OnMouseMove()
{
  int x = this->Interactor->GetEventPosition()[0];
  int y = this->Interactor->GetEventPosition()[1];

  switch (this->State)
    {
    case VTKIS_ROTATE:
      this->FindPokedRenderer(x, y);
      this->Rotate();
      this->InvokeEvent(vtkCommand::InteractionEvent, NULL);
      break;
    case VTKIS_DOLLY:
      this->FindPokedRenderer(x, y);
      this->Dolly();
      this->InvokeEvent(vtkCommand::InteractionEvent, NULL);
      break;
    }
}

//----------------------------------------------------------------------------
void vtkInteractorStyleOpenVR::OnLeftButtonDown()
{
  int x = this->Interactor->GetEventPosition()[0];
  int y = this->Interactor->GetEventPosition()[1];

  vtkOpenVRRenderWindowInteractor *vriren =
    vtkOpenVRRenderWindowInteractor::SafeDownCast(this->Interactor);

  double *wpos = vriren->GetWorldEventPosition(
    vriren->GetPointerIndex());

  this->FindPokedRenderer(x, y);
  this->FindPickedActor(wpos[0], wpos[1], wpos[2]);
  if (this->CurrentRenderer == NULL || this->InteractionProp == NULL)
    {
    return;
    }

  this->GrabFocus(this->EventCallbackCommand);
  this->StartRotate();
}

//----------------------------------------------------------------------------
void vtkInteractorStyleOpenVR::OnLeftButtonUp()
{
  switch (this->State)
    {
    // in our case roate state is used for actor pose adjustments
    case VTKIS_ROTATE:
      this->EndRotate();
      break;
    }

  if ( this->Interactor )
    {
    this->ReleaseFocus();
    }
}

//----------------------------------------------------------------------------
// We handle all adjustments here
void vtkInteractorStyleOpenVR::Rotate()
{
  if (this->CurrentRenderer == NULL || this->InteractionProp == NULL)
    {
    return;
    }

  vtkOpenVRRenderWindowInteractor *rwi =
    static_cast<vtkOpenVRRenderWindowInteractor *>(this->Interactor);

  double *wpos = rwi->GetWorldEventPosition(
    rwi->GetPointerIndex());

  double *lwpos = rwi->GetLastWorldEventPosition(
    rwi->GetPointerIndex());

  double trans[3];
  for (int i = 0; i < 3; i++)
    {
    trans[i] = wpos[i] - lwpos[i];
    }

  // vtkVector3<double> p1;
  // vtkVector3<double> p2;
  // p2.Subtract(p1);

  if (this->InteractionProp->GetUserMatrix() != NULL)
    {
    vtkTransform *t = vtkTransform::New();
    t->PostMultiply();
    t->SetMatrix(this->InteractionProp->GetUserMatrix());
    t->Translate(trans);
    this->InteractionProp->GetUserMatrix()->DeepCopy(t->GetMatrix());
    t->Delete();
    }
  else
    {
    this->InteractionProp->AddPosition(trans);
    }

  double *wori = rwi->GetWorldEventOrientation(
    rwi->GetPointerIndex());

  double *lwori = rwi->GetLastWorldEventOrientation(
    rwi->GetPointerIndex());

  // compute the net rotation
  vtkQuaternion<double> q1;
  q1.SetRotationAngleAndAxis(
    vtkMath::RadiansFromDegrees(lwori[0]), lwori[1], lwori[2], lwori[3]);
  vtkQuaternion<double> q2;
  q2.SetRotationAngleAndAxis(
    vtkMath::RadiansFromDegrees(wori[0]), wori[1], wori[2], wori[3]);
  q1.Conjugate();
  q2 = q2*q1;
  double axis[4];
  axis[0] = vtkMath::DegreesFromRadians(q2.GetRotationAngleAndAxis(axis+1));

  double scale[3];
  scale[0] = scale[1] = scale[2] = 1.0;

  double *rotate = axis;
  this->Prop3DTransform(this->InteractionProp,
                        wpos,
                        1,
                        &rotate,
                        scale);

  if (this->AutoAdjustCameraClippingRange)
    {
    this->CurrentRenderer->ResetCameraClippingRange();
    }
}

//----------------------------------------------------------------------------
void vtkInteractorStyleOpenVR::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
void vtkInteractorStyleOpenVR::FindPickedActor(double x, double y, double z)
{
  this->InteractionPicker->Pick(x, y, z, this->CurrentRenderer);
  vtkProp *prop = this->InteractionPicker->GetViewProp();
  if (prop != NULL)
    {
    this->InteractionProp = vtkProp3D::SafeDownCast(prop);
    }
  else
    {
    this->InteractionProp = NULL;
    }
}

//----------------------------------------------------------------------------
void vtkInteractorStyleOpenVR::Prop3DTransform(vtkProp3D *prop3D,
                                                       double *boxCenter,
                                                       int numRotation,
                                                       double **rotate,
                                                       double *scale)
{
  vtkMatrix4x4 *oldMatrix = vtkMatrix4x4::New();
  prop3D->GetMatrix(oldMatrix);

  double orig[3];
  prop3D->GetOrigin(orig);

  vtkTransform *newTransform = vtkTransform::New();
  newTransform->PostMultiply();
  if (prop3D->GetUserMatrix() != NULL)
    {
    newTransform->SetMatrix(prop3D->GetUserMatrix());
    }
  else
    {
    newTransform->SetMatrix(oldMatrix);
    }

  newTransform->Translate(-(boxCenter[0]), -(boxCenter[1]), -(boxCenter[2]));

  for (int i = 0; i < numRotation; i++)
    {
    newTransform->RotateWXYZ(rotate[i][0], rotate[i][1],
                             rotate[i][2], rotate[i][3]);
    }

  if ((scale[0] * scale[1] * scale[2]) != 0.0)
    {
    newTransform->Scale(scale[0], scale[1], scale[2]);
    }

  newTransform->Translate(boxCenter[0], boxCenter[1], boxCenter[2]);

  // now try to get the composit of translate, rotate, and scale
  newTransform->Translate(-(orig[0]), -(orig[1]), -(orig[2]));
  newTransform->PreMultiply();
  newTransform->Translate(orig[0], orig[1], orig[2]);

  if (prop3D->GetUserMatrix() != NULL)
    {
    newTransform->GetMatrix(prop3D->GetUserMatrix());
    }
  else
    {
    prop3D->SetPosition(newTransform->GetPosition());
    prop3D->SetScale(newTransform->GetScale());
    prop3D->SetOrientation(newTransform->GetOrientation());
    }
  oldMatrix->Delete();
  newTransform->Delete();
}

//----------------------------------------------------------------------------
void vtkInteractorStyleOpenVR::OnRightButtonDown()
{
  int x = this->Interactor->GetEventPosition()[0];
  int y = this->Interactor->GetEventPosition()[1];

  this->FindPokedRenderer(x, y);
  if (this->CurrentRenderer == NULL)
    {
    return;
    }

  this->GrabFocus(this->EventCallbackCommand);
  this->StartDolly();
}

//----------------------------------------------------------------------------
void vtkInteractorStyleOpenVR::OnRightButtonUp()
{
  switch (this->State)
    {
    // in our case roate state is used for all adjustments
    case VTKIS_DOLLY:
      this->EndDolly();
      break;
    }

  if ( this->Interactor )
    {
    this->ReleaseFocus();
    }
}

void vtkInteractorStyleOpenVR::Dolly()
{
  if (this->CurrentRenderer == NULL)
    {
    return;
    }

  vtkOpenVRRenderWindowInteractor *rwi =
    static_cast<vtkOpenVRRenderWindowInteractor *>(this->Interactor);

  double *wori = rwi->GetWorldEventOrientation(
    rwi->GetPointerIndex());

  // move HMD world in the direction of the controller
  vtkQuaternion<double> q1;
  q1.SetRotationAngleAndAxis(
    vtkMath::RadiansFromDegrees(wori[0]), wori[1], wori[2], wori[3]);

  double elem[3][3];
  q1.ToMatrix3x3(elem);
  double vdir[3] = {0.0,0.0,-1.0};
  vtkMatrix3x3::MultiplyPoint(
    elem[0],vdir,vdir);

  vtkOpenVRCamera* cam =
    static_cast<vtkOpenVRCamera *>( this->CurrentRenderer->GetActiveCamera());
  double *trans = cam->GetTranslation();
  double scale = cam->GetScale();

  // move at a max rate of 0.1 meters per frame in HMD space
  // this works out to about 10 meters per second
  // which is quick. The world coordinate speed of
  // movement can be determined from the camera scale.
  // movement speed is scaled by the touchpad
  // y coordinate

  float *tpos = rwi->GetTouchPadPosition();
  cam->SetTranslation(
    trans[0]-vdir[0]*0.05*tpos[1]/scale,
    trans[1]-vdir[1]*0.05*tpos[1]/scale,
    trans[2]-vdir[2]*0.05*tpos[1]/scale);

  if (this->AutoAdjustCameraClippingRange)
    {
    this->CurrentRenderer->ResetCameraClippingRange();
    }
}
